﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_48.Controllers
{
    public class ReportJoinController : Controller
    {
        // GET: ReportJoin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Employee2()
        {
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            string urlReportServer = "http://localhost/ReportServer";
            report.ProcessingMode = ProcessingMode.Remote;
            report.ServerReport.ReportServerUrl = new Uri(urlReportServer);
            report.ServerReport.ReportPath = "/Report_Employee2";
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}